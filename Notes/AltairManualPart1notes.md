# Part 1 Notes

#### A. Logic Notes
<p>all practical digital computers and many other electronic circuits are based upon the logic concepts explained by Boole.<br>
**a logic condition or statement is either true or false. It cannot be both true and false, and it cannot be partially true or partially false**<br>
There are three basic logic statements:</p>
- AND
	- True if and only if either or all of its logic conditions are true.
- OR
	- True if at least one logic condition is true
- NOT
	- Reverses the meaning of a logic statement so that a true statement is false and a false statement is true.
<p>Switch ON is true/1, switch OFF is false/0</p>

#### B. Electronic Logic Notes
<p>The three basic logic circuits can be combined with one another to produce more logic statement analogies<br>
These combinations have their own logic symbols and truth tables</p>
- NAND
	- True if all logic conditions are not the same 
- NOR
	- True if all logic conditions are false
- XOR
	- Only true if one statement is true and one is false
<p>The XOR function is used to represent SUM, it is known as a binary adder<br>
two NAND circuits can be connected to form a bistable circuit called a flip-flop<br>
flip-flop changes state when a pulse arrives, it acts as a short term memory element<br> 
several flip-flops can be cascaded together to form electronic counters and memory registers</p>

#### C. Number Systems Notes
<p>Humans use number systems that consist of 10 digits, however, number systems can be based on any number of digits<br>
dual-state electronic circuits are highly compatible with two digit number systems, digits are called **bits**</p>

#### D. The Binary System Notes
<p>The Altair performs nearly all operations in binary<br>
a typical binary number and consists of 0's and 1's, looks like this: 10111010<br>
a fixed length bianry number like this is called a **byte**<br>
The binary equivalent of the decimal 0 is 0. Similarly, the binary equivalent of the decimal 1 is 1.<br>
decimal 10 is 10, binary 10 is decimal 2<br>
Each bit in a binary number indicates by which power of two the number js to be raised. The sum of the powers of two gives the decimal equivalent for the number.</p>

